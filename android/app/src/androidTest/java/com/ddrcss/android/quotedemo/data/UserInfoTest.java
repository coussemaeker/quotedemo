package com.ddrcss.android.quotedemo.data;

import com.ddrcss.android.quotedemo.tools.RandomGen;
import com.ddrcss.android.quotedemo.tools.Static;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class UserInfoTest {

    private UserInfo userInfo;
    private String login;
    private String picUrl;
    private int favoriteCount;
    private int followers;
    private int following;

    @Before
    public void setUp() throws Exception {
        JsonObject jsonUserInfo = new JsonObject();
        login = RandomGen.getString();
        jsonUserInfo.addProperty("login", login);
        picUrl = RandomGen.getString();
        jsonUserInfo.addProperty("pic_url", picUrl);
        favoriteCount = RandomGen.getRandom().nextInt();
        jsonUserInfo.addProperty("public_favorites_count", favoriteCount);
        followers = RandomGen.getRandom().nextInt();
        jsonUserInfo.addProperty("followers", followers);
        following = RandomGen.getRandom().nextInt();
        jsonUserInfo.addProperty("following", following);
        jsonUserInfo.addProperty("pro", true);

        String json = new GsonBuilder().create().toJson(jsonUserInfo);
        userInfo = Static.getGson().fromJson(json, UserInfo.class);
    }

    @Test
    public void getLogin() {
        assertEquals(userInfo.getLogin(), login);
    }

    @Test
    public void getPicUrl() {
        assertEquals(userInfo.getPicUrl(), picUrl);
    }

    @Test
    public void getPublicFavoritesCount() {
        assertEquals(userInfo.getPublicFavoritesCount(), favoriteCount);
    }

    @Test
    public void getFollowers() {
        assertEquals(userInfo.getFollowers(), followers);
    }

    @Test
    public void getFollowing() {
        assertEquals(userInfo.getFollowing(), following);
    }

    @Test
    public void isPro() {
        assertTrue(userInfo.isPro());
    }

}

package com.ddrcss.android.quotedemo.data;

import com.ddrcss.android.quotedemo.tools.RandomGen;
import com.ddrcss.android.quotedemo.tools.Static;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class QuoteTest {

    private Quote quote;
    private String author;
    private String body;
    private String fistTag;
    private boolean favorite;
    private long quoteId;
    private int favoriteCount;
    private String url;

    @Before
    public void setUp() throws Exception {
        JsonObject jsonQuote = new JsonObject();
        author = RandomGen.getString();
        jsonQuote.addProperty("author", author);
        body = RandomGen.getString();
        jsonQuote.addProperty("body", body);
        fistTag = RandomGen.getString();
        JsonArray tags = new JsonArray();
        tags.add(fistTag);
        jsonQuote.add("tags", tags);
        favorite = true;
        jsonQuote.addProperty("favorite", favorite);
        quoteId = RandomGen.getRandom().nextLong();
        jsonQuote.addProperty("id", quoteId);
        favoriteCount = RandomGen.getRandom().nextInt();
        jsonQuote.addProperty("favorites_count", favoriteCount);
        url = RandomGen.getString();
        jsonQuote.addProperty("url", url);
        String json = new GsonBuilder().create().toJson(jsonQuote);
        quote = Static.getGson().fromJson(json, Quote.class);
    }

    @Test
    public void getAuthor() {
        assertEquals(quote.getAuthor(), author);
    }

    @Test
    public void getBody() {
        assertEquals(quote.getBody(), body);
    }

    @Test
    public void getTags() {
        assertEquals(quote.getTags().size(), 1);
        assertEquals(quote.getTags().get(0), fistTag);
    }

    @Test
    public void isFavorite() {
        assertEquals(quote.isFavorite(), favorite);
    }

    @Test
    public void getQuoteId() {
        assertEquals(quote.getQuoteId(), quoteId);
    }

    @Test
    public void getFavoriteCount() {
        assertEquals(quote.getFavoriteCount(), favoriteCount);
    }

    @Test
    public void getUrl() {
        assertEquals(quote.getUrl(), url);
    }

}

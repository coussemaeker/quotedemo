package com.ddrcss.android.quotedemo.tools;

import android.content.Context;
import android.support.test.InstrumentationRegistry;

import org.junit.Test;

import static org.junit.Assert.*;

public class PrefsStorageTest {

    @Test
    public void getLogin() {
        Context appContext = InstrumentationRegistry.getTargetContext();
        PrefsStorage storage = new PrefsStorage(appContext);
        storage.dismiss();
        String login = RandomGen.getString();
        String token = RandomGen.getString();
        storage.save(login, token);
        assertEquals(login, storage.getLogin());
    }

    @Test
    public void getToken() {
        Context appContext = InstrumentationRegistry.getTargetContext();
        PrefsStorage storage = new PrefsStorage(appContext);
        storage.dismiss();
        String login = RandomGen.getString();
        String token = RandomGen.getString();
        storage.save(login, token);
        assertEquals(token, storage.getToken());
    }

    @Test
    public void hasSessionInfo1() {
        Context appContext = InstrumentationRegistry.getTargetContext();
        PrefsStorage storage = new PrefsStorage(appContext);
        storage.dismiss();
        assertFalse(storage.hasSessionInfo());
    }

    @Test
    public void hasSessionInfo2() {
        Context appContext = InstrumentationRegistry.getTargetContext();
        PrefsStorage storage = new PrefsStorage(appContext);
        storage.dismiss();
        String login = RandomGen.getString();
        String token = RandomGen.getString();
        storage.save(login, token);
        assertTrue(storage.hasSessionInfo());
    }

    @Test
    public void dismiss() {
        Context appContext = InstrumentationRegistry.getTargetContext();
        PrefsStorage storage = new PrefsStorage(appContext);
        String login = RandomGen.getString();
        String token = RandomGen.getString();
        storage.save(login, token);
        storage.dismiss();
        assertNull(storage.getLogin());
        assertNull(storage.getToken());
    }
}
package com.ddrcss.android.quotedemo.tools;

import java.util.Random;

public class RandomGen {

    private static Random random = new Random(System.currentTimeMillis());

    public static String getString() {
        return Integer.toString(random.nextInt(), 36);
    }

    public static Random getRandom() {
        return random;
    }

}

package com.ddrcss.android.quotedemo.tools;

import android.content.Context;
import android.support.test.InstrumentationRegistry;

import com.ddrcss.android.quotedemo.data.Quote;
import com.ddrcss.android.quotedemo.data.QuoteCacheEntry;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class FileCacheTest {

    private Context appContext;
    private FileCache fileCache;

    private void deleteAllFiles() {
        File cacheDir = new File(appContext.getCacheDir(), "quotecache");
        File[] children = cacheDir.listFiles();
        for (File file : children) {
            file.delete();
        }
    }

    private Quote createRandomQuote() {
        JsonObject jsonQuote = new JsonObject();
        String author = RandomGen.getString();
        jsonQuote.addProperty("author", author);
        String body = RandomGen.getString();
        jsonQuote.addProperty("body", body);
        String fistTag = RandomGen.getString();
        JsonArray tags = new JsonArray();
        tags.add(fistTag);
        jsonQuote.add("tags", tags);
        boolean favorite = true;
        jsonQuote.addProperty("favorite", favorite);
        long quoteId = RandomGen.getRandom().nextLong();
        jsonQuote.addProperty("id", quoteId);
        int favoriteCount = RandomGen.getRandom().nextInt();
        jsonQuote.addProperty("favorites_count", favoriteCount);
        String url = RandomGen.getString();
        jsonQuote.addProperty("url", url);
        String json = new GsonBuilder().create().toJson(jsonQuote);
        return Static.getGson().fromJson(json, Quote.class);
    }

    @Before
    public void setUp() throws Exception {
        appContext = InstrumentationRegistry.getTargetContext();
        fileCache = new FileCache(appContext);
    }

    @Test
    public void save() {
        String login = RandomGen.getString();

        int count = RandomGen.getRandom().nextInt(500);
        List<Quote> quotes = new ArrayList<>(count);
        for (int i = 0; i < count; i++) {
            quotes.add(createRandomQuote());
        }
        deleteAllFiles();
        QuoteCacheEntry cacheEntry = new QuoteCacheEntry(quotes);
        fileCache.save(login, cacheEntry);
        assertNotNull(fileCache.load(login));
    }

    @Test
    public void load() {
        String login = RandomGen.getString();

        int count = RandomGen.getRandom().nextInt(500);
        List<Quote> quotes = new ArrayList<>(count);
        for (int i = 0; i < count; i++) {
            quotes.add(createRandomQuote());
        }
        deleteAllFiles();
        QuoteCacheEntry writtenEntry = new QuoteCacheEntry(quotes);
        fileCache.save(login, writtenEntry);
        QuoteCacheEntry readEntry = fileCache.load(login);
        assertEquals(readEntry.getQuotes().size(), count);
        for (int i =0; i < count; i++) {
            assertEquals(writtenEntry.getQuotes().get(i).getQuoteId(), readEntry.getQuotes().get(i).getQuoteId());
            assertEquals(writtenEntry.getQuotes().get(i).getAuthor(), readEntry.getQuotes().get(i).getAuthor());
            assertEquals(writtenEntry.getQuotes().get(i).getBody(), readEntry.getQuotes().get(i).getBody());
        }
    }
}
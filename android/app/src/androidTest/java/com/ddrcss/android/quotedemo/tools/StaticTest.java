package com.ddrcss.android.quotedemo.tools;

import android.content.Context;
import android.support.test.InstrumentationRegistry;

import com.google.gson.Gson;

import org.junit.Test;

import okhttp3.OkHttpClient;

import static org.junit.Assert.*;

public class StaticTest {

    @Test
    public void init() {
        Context appContext = InstrumentationRegistry.getTargetContext();
        Static.init(appContext);
        assertTrue(Static.getFileCache() instanceof FileCache);
        assertTrue(Static.getGson() instanceof Gson);
        assertTrue(Static.getOkHttpClient() instanceof OkHttpClient);
        assertTrue(Static.getPrefsStorage() instanceof PrefsStorage);
    }

    @Test
    public void getOkHttpClient() {
        Context appContext = InstrumentationRegistry.getTargetContext();
        Static.init(appContext);
        assertNotNull(Static.getOkHttpClient());
        assertSame(Static.getOkHttpClient(), Static.getOkHttpClient());
    }

    @Test
    public void getGson() {
        Context appContext = InstrumentationRegistry.getTargetContext();
        Static.init(appContext);
        assertNotNull(Static.getGson());
        assertSame(Static.getGson(), Static.getGson());
    }

    @Test
    public void getPrefsStorage() {
        Context appContext = InstrumentationRegistry.getTargetContext();
        Static.init(appContext);
        assertNotNull(Static.getPrefsStorage());
        assertSame(Static.getPrefsStorage(), Static.getPrefsStorage());
    }

    @Test
    public void getFileCache() {
        Context appContext = InstrumentationRegistry.getTargetContext();
        Static.init(appContext);
        assertNotNull(Static.getFileCache());
        assertSame(Static.getFileCache(), Static.getFileCache());
    }

}
package com.ddrcss.android.quotedemo.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;

import com.ddrcss.android.quotedemo.DemoActivity;

public class DemoFragment extends Fragment {

    public DemoFragment() {
        setArguments(new Bundle());
    }

    public DemoActivity getParentActivity() {
        FragmentActivity activity = getActivity();
        if (activity instanceof DemoActivity) {
            return (DemoActivity) activity;
        } else {
            throw new IllegalStateException("unsupported parent activity");
        }
    }

}

package com.ddrcss.android.quotedemo.tools;

import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import okhttp3.OkHttpClient;

public class Static {

    private Static() {
        // force static use
    }

    private static OkHttpClient okHttpClient;
    private static Gson gson;
    private static PrefsStorage prefsStorage;
    private static FileCache fileCache;

    public static void init(Context context) {
        okHttpClient = new OkHttpClient.Builder().build();
        gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
        prefsStorage = new PrefsStorage(context);
        fileCache = new FileCache(context);
    }

    public static OkHttpClient getOkHttpClient() {
        return okHttpClient;
    }

    public static Gson getGson() {
        return gson;
    }

    public static PrefsStorage getPrefsStorage() {
        return prefsStorage;
    }

    public static FileCache getFileCache() {
        return fileCache;
    }

}

package com.ddrcss.android.quotedemo.fragment;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.DragEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import com.ddrcss.android.quotedemo.R;
import com.ddrcss.android.quotedemo.data.Quote;
import com.ddrcss.android.quotedemo.data.QuoteCacheEntry;
import com.ddrcss.android.quotedemo.data.QuotesPage;
import com.ddrcss.android.quotedemo.network.ApiCall;
import com.ddrcss.android.quotedemo.network.QuotesCall;
import com.ddrcss.android.quotedemo.tools.Static;
import com.ddrcss.android.quotedemo.ui.QuoteListAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * Displays a list of quotes
 */
public class QuotesFragment extends DemoFragment {

    private String login;
    private String token;
    private ListView quoteList;
    private QuoteListAdapter quoteListAdapter;
    private View scrollUp;
    private View scrollDown;

    public QuotesFragment() {
        super();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_quotes, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        quoteList = view.findViewById(R.id.quoteList);
        scrollUp = view.findViewById(R.id.scrollUpArea);
        scrollDown = view.findViewById(R.id.scrollDownArea);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (login == null) {
            login = Static.getPrefsStorage().getLogin();
            token = Static.getPrefsStorage().getToken();
            if (login == null || token == null) {
                getParentActivity().loadUserSigninFragment();
                return;
            }
        }
        if (quoteListAdapter == null) {
            loadQuotes(0);
        }

        View.OnDragListener onDragListener = new View.OnDragListener() {
            @Override
            public boolean onDrag(View view, DragEvent dragEvent) {
                switch (dragEvent.getAction()) {
                    case DragEvent.ACTION_DRAG_LOCATION:
                        int scrollStep = 0;
                        if (view == scrollUp) {
                            scrollStep = -1;
                        } else if (view == scrollDown) {
                            scrollStep = 1;
                        }
                        quoteList.smoothScrollByOffset(scrollStep);
                        break;

                    case DragEvent.ACTION_DRAG_ENDED:
                        view.setVisibility(View.GONE);
                        break;
                }
                return true;
            }
        };
        scrollUp.setOnDragListener(onDragListener);
        scrollDown.setOnDragListener(onDragListener);
    }

    private void loadQuotes(int pageNum) {
        new QuotesCall(login, token, pageNum).execute(this::onPageLoaded, (ioe, errorResult) -> {
            Toast.makeText(getActivity(), ApiCall.formatError(ioe, errorResult), Toast.LENGTH_LONG).show();
            loadFromCache();
        });
    }

    private void onPageLoaded(QuotesPage quotesPage) {
        if (quoteListAdapter == null) {
            quoteListAdapter = new QuoteListAdapter(getParentActivity(), quotesPage.getQuotes());
            quoteList.setAdapter(quoteListAdapter);
            quoteListAdapter.setScrollAreas(scrollUp, scrollDown);
        } else {
            quoteListAdapter.appendItems(quotesPage.getQuotes());
        }
        if (!quotesPage.isLastPage()) {
            loadQuotes(quotesPage.getPageNumber() + 1);
        } else {
            saveToCache();
        }
    }

    private void saveToCache() {
        int count = quoteListAdapter.getCount();
        List<Quote> items = new ArrayList<>(count);
        for (int i = 0; i < count; i++) {
            if (quoteListAdapter.getItem(i) instanceof Quote) {
                items.add((Quote) quoteListAdapter.getItem(i));
            }
        }
        QuoteCacheEntry cacheEntry = new QuoteCacheEntry(items);
        new Thread(() -> Static.getFileCache().save(login, cacheEntry)).start();
    }

    private void loadFromCache() {
        new AsyncTask<String, Void, QuoteCacheEntry>() {
            @Override
            protected QuoteCacheEntry doInBackground(String... logins) {
                if (logins.length > 0) {
                    return Static.getFileCache().load(logins[0]);
                } else {
                    return null;
                }
            }

            @Override
            protected void onPostExecute(QuoteCacheEntry cacheEntry) {
                if (cacheEntry != null) {
                    if (quoteListAdapter == null) {
                        quoteListAdapter = new QuoteListAdapter(getParentActivity(), cacheEntry.getQuotes());
                        quoteList.setAdapter(quoteListAdapter);
                    } else {
                        quoteListAdapter.clear();
                        quoteListAdapter.appendItems(cacheEntry.getQuotes());
                    }

                }
            }
        }.execute(login);
    }

}

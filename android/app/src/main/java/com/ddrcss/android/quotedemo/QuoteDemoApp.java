package com.ddrcss.android.quotedemo;

import android.app.Application;

import com.ddrcss.android.quotedemo.tools.Static;

public class QuoteDemoApp extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        Static.init(this);
    }
}

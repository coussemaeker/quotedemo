package com.ddrcss.android.quotedemo.data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SessionPayload {

    private static final String PROPERTY_USER = "user";
    private static final String PROPERTY_LOGIN = "login";
    private static final String PROPERTY_PASSWORD = "password";

    @Expose
    @SerializedName(PROPERTY_USER)
    private User user;

    private static class User {
        @Expose
        @SerializedName(PROPERTY_LOGIN)
        public String username;

        @Expose
        @SerializedName(PROPERTY_PASSWORD)
        public String password;

    }

    public SessionPayload(String username, String password) {
        this.user = new User();
        this.user.username = username;
        this.user.password = password;
    }

    public String getUsername() {
        return user.username;
    }

    public String getPassword() {
        return user.password;
    }

}

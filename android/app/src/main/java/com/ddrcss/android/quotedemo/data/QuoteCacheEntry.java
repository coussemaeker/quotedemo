package com.ddrcss.android.quotedemo.data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class QuoteCacheEntry {

    private static final String PROPERTY_TIMESTAMP = "timestamp";
    private static final String PROPERTY_QUOTES = "quotes";

    public QuoteCacheEntry(List<Quote> quotes) {
        this.timestamp = System.currentTimeMillis();
        this.quotes = new ArrayList<>(quotes);
    }

    @Expose
    @SerializedName(PROPERTY_TIMESTAMP)
    private long timestamp;

    @Expose
    @SerializedName(PROPERTY_QUOTES)
    private List<Quote> quotes;

    public long getTimestamp() {
        return timestamp;
    }

    public List<Quote> getQuotes() {
        return quotes;
    }

}

package com.ddrcss.android.quotedemo.data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UserInfo {

    private final static String PROPERTY_LOGIN = "login";
    private final static String PROPERTY_PIC_URL = "pic_url";
    private final static String PROPERTY_PUBLIC_FAVORITES_COUNT = "public_favorites_count";
    private final static String PROPERTY_FOLLOWERS = "followers";
    private final static String PROPERTY_FOLLOWING = "following";
    private final static String PROPERTY_PRO = "pro";

    @Expose
    @SerializedName(PROPERTY_LOGIN)
    private String login;

    @Expose
    @SerializedName(PROPERTY_PIC_URL)
    private String picUrl;

    @Expose
    @SerializedName(PROPERTY_PUBLIC_FAVORITES_COUNT)
    private int publicFavoritesCount;

    @Expose
    @SerializedName(PROPERTY_FOLLOWERS)
    private int followers;

    @Expose
    @SerializedName(PROPERTY_FOLLOWING)
    private int following;

    @Expose
    @SerializedName(PROPERTY_PRO)
    private boolean pro;

    public String getLogin() {
        return login;
    }

    public String getPicUrl() {
        return picUrl;
    }

    public int getPublicFavoritesCount() {
        return publicFavoritesCount;
    }

    public int getFollowers() {
        return followers;
    }

    public int getFollowing() {
        return following;
    }

    public boolean isPro() {
        return pro;
    }

}

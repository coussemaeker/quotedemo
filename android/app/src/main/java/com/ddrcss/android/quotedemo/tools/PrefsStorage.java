package com.ddrcss.android.quotedemo.tools;

import android.content.Context;
import android.content.SharedPreferences;

public class PrefsStorage {

    private static final String PREFS_NAME = "FAVQS_USER";

    private static final String KEY_LOGIN = "login";
    private static final String KEY_TOKEN = "token";

    private SharedPreferences preferences;

    public PrefsStorage(Context context) {
        preferences = context.getSharedPreferences(PREFS_NAME, 0);
    }

    public String getLogin() {
        return preferences.getString(KEY_LOGIN, null);
    }

    public String getToken() {
        return preferences.getString(KEY_TOKEN, null);
    }

    public boolean hasSessionInfo() {
        return getLogin() != null && getToken() != null;
    }

    public void save(String login, String token) {
        preferences.edit()
                .putString(KEY_LOGIN, login)
                .putString(KEY_TOKEN, token)
                .apply();
    }

    public void dismiss() {
        preferences.edit()
                .remove(KEY_LOGIN)
                .remove(KEY_TOKEN)
                .apply();
    }

}

package com.ddrcss.android.quotedemo.ui;

import android.app.Activity;
import android.content.ClipData;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.DragEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.ddrcss.android.quotedemo.R;
import com.ddrcss.android.quotedemo.data.AbstractQuote;
import com.ddrcss.android.quotedemo.data.Quote;

import java.util.ArrayList;
import java.util.List;

public class QuoteListAdapter extends ArrayAdapter<AbstractQuote> {

    private ArrayList<AbstractQuote> quoteItems;
    private LayoutInflater layoutInflater;

    private View[] scrollAreas = {};

    public QuoteListAdapter(Activity activity, List<Quote> quotes) {
        super(activity, 0, new ArrayList<>(quotes));
        layoutInflater = activity.getLayoutInflater();
    }

    private static class ViewHolder {
        public int position;
        public TextView authorText;
        public ViewGroup bodyContainer;
        public TextView favCountText;
    }

    public void setScrollAreas(View... scrollAreas) {
        this.scrollAreas = scrollAreas;
    }

    @Override
    public boolean areAllItemsEnabled() {
        return true;
    }

    @Override
    public int getViewTypeCount() {
        return 1;
    }

    @Override
    public int getItemViewType(int position) {
        return 0;
    }

    public void appendItems(List<Quote> items) {
        super.addAll(items);
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        ViewHolder viewHolder;
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.item_favorite_quote, parent, false);
            convertView.setClickable(true);
            convertView.setTag(viewHolder = new ViewHolder());
            viewHolder.authorText = convertView.findViewById(R.id.quote_author);
            viewHolder.bodyContainer = convertView.findViewById(R.id.quote_body_container);
            viewHolder.favCountText = convertView.findViewById(R.id.quote_fav_count);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        AbstractQuote quoteItem = getItem(position);
        viewHolder.position = position;
        viewHolder.authorText.setText(quoteItem.getAuthor());
        quoteItem.bindBodyToView(layoutInflater, viewHolder.bodyContainer);
        if (quoteItem.getFavoriteCount() > 1) {
            viewHolder.favCountText.setVisibility(View.VISIBLE);
            viewHolder.favCountText.setText(getContext().getString(R.string.text_fav_count, Integer.toString(quoteItem.getFavoriteCount())));
        } else {
            viewHolder.favCountText.setVisibility(View.GONE);
            viewHolder.favCountText.setText(null);
        }

        // drag and drop
        convertView.setOnDragListener(new View.OnDragListener() {
            @Override
            public boolean onDrag(View view, DragEvent dragEvent) {

                /*
                String action = "unknown";
                switch (dragEvent.getAction()) {
                    case DragEvent.ACTION_DRAG_ENDED:
                        action = "ended";
                        break;

                    case DragEvent.ACTION_DRAG_ENTERED:
                        action = "entered";
                        break;

                    case DragEvent.ACTION_DRAG_EXITED:
                        action = "exited";
                        break;

                    case DragEvent.ACTION_DRAG_LOCATION:
                        action = "location";
                        break;

                    case DragEvent.ACTION_DRAG_STARTED:
                        action = "started";
                        break;

                    case DragEvent.ACTION_DROP:
                        action = "drop";
                        break;
                }
                Log.d(view.getClass().getName(), "onDrag event action=" + action + " srcPos=" + srcPos + " destPos=" + destPos);
                */
                int srcPos = (Integer) dragEvent.getLocalState();
                int destPos = -1;
                Object tag = view.getTag();
                if (tag instanceof ViewHolder) {
                    destPos = ((ViewHolder) tag).position;
                }
                if (dragEvent.getAction() == DragEvent.ACTION_DROP) {
                    moveItem(srcPos, destPos);
                }
                return true;
            }
        });

        convertView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                ClipData clipData = ClipData.newPlainText("label", "text");
                for (View scrollArea : scrollAreas) {
                    scrollArea.setVisibility(View.VISIBLE);
                }
                view.startDrag(clipData, new CustomDragShadowBuilder(view), Integer.valueOf(position), 0);
                return true;
            }
        });

        return convertView;
    }

    private void moveItem(int fromPosition, int toPosition) {
        if (fromPosition == toPosition) {
            return;
        }
        AbstractQuote quoteItem = getItem(fromPosition);
        remove(quoteItem);
        insert(quoteItem, toPosition);
        notifyDataSetChanged();
    }

    private static class CustomDragShadowBuilder extends View.DragShadowBuilder {

        private static Drawable shadow;

        public CustomDragShadowBuilder(View view) {
            super(view);
            shadow = new ColorDrawable(Color.LTGRAY);
        }

        @Override
        public void onProvideShadowMetrics(Point size, Point touch) {
            int width, height;

            width = getView().getWidth() / 2;
            height = getView().getHeight() / 2;
            shadow.setBounds(0, 0, width, height);

            size.set(width, height);
            touch.set(width, height);
        }

        @Override
        public void onDrawShadow(Canvas canvas) {

            shadow.draw(canvas);
            super.onDrawShadow(canvas);
        }
    }

}

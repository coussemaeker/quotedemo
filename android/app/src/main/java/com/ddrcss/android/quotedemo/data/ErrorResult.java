package com.ddrcss.android.quotedemo.data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ErrorResult {

    private static final String PROPERTY_ERROR_CODE = "error_code";
    private static final String PROPERTY_MESSAGE = "message";

    @Expose
    @SerializedName(PROPERTY_ERROR_CODE)
    private Integer errorCode;

    @Expose
    @SerializedName(PROPERTY_MESSAGE)
    private String message;

    public boolean isError() {
        return errorCode != null;
    }

    public int getErrorCode() {
        return errorCode != null ? errorCode.intValue() : -1;
    }

    public String getMessage() {
        return message;
    }

}

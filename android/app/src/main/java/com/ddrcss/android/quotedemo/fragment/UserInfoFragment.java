package com.ddrcss.android.quotedemo.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.ddrcss.android.quotedemo.R;
import com.ddrcss.android.quotedemo.data.UserInfo;
import com.ddrcss.android.quotedemo.network.ApiCall;
import com.ddrcss.android.quotedemo.network.UserInfoCall;
import com.ddrcss.android.quotedemo.tools.PrefsStorage;
import com.ddrcss.android.quotedemo.tools.Static;

public class UserInfoFragment extends DemoFragment {

    private TextView loginText;
    private TextView favoriteText;
    private TextView followersText;
    private TextView followingText;
    private Button favoriteQuotesButton;
    private Button signoutButton;

    private PrefsStorage prefsStorage;
    private UserInfo userInfo;

    public UserInfoFragment() {
        super();
        prefsStorage = Static.getPrefsStorage();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_user_info, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        loginText = view.findViewById(R.id.loginText);
        favoriteText = view.findViewById(R.id.favCountText);
        followersText = view.findViewById(R.id.followersText);
        followingText = view.findViewById(R.id.followingText);
        favoriteQuotesButton = view.findViewById(R.id.favoriteQuotesButton);
        favoriteQuotesButton.setOnClickListener(this::favoriteQuotesPressed);
        signoutButton = view.findViewById(R.id.signoutButton);
        signoutButton.setOnClickListener(this::signoutPressed);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (prefsStorage.hasSessionInfo()) {
            loadUserInfo();
        } else {
            getParentActivity().loadUserSigninFragment();
        }
    }

    private void signoutPressed(View buttonView) {
        prefsStorage.dismiss();
        getParentActivity().loadUserSigninFragment();
    }

    private void favoriteQuotesPressed(View buttonView) {
        getParentActivity().loadQuotesFragment();
    }

    private void loadUserInfo() {
        String login = prefsStorage.getLogin();
        loginText.setText(getString(R.string.text_login, login));
        new UserInfoCall(login, prefsStorage.getToken()).execute(this::userInfoLoaded, (ioe, errorResult) -> {
            Toast.makeText(getActivity(), "Error loading user info\n" + ApiCall.formatError(ioe, errorResult), Toast.LENGTH_LONG).show();
        });
    }

    private void userInfoLoaded(UserInfo userInfo) {
        favoriteText.setText(getString(R.string.text_favorite_count, Integer.toString(userInfo.getPublicFavoritesCount())));
        followersText.setText(getString(R.string.text_followers_count, Integer.toString(userInfo.getFollowers())));
        followingText.setText(getString(R.string.text_following_count, Integer.toString(userInfo.getFollowing())));
    }

}

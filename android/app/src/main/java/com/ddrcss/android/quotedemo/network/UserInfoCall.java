package com.ddrcss.android.quotedemo.network;

import com.ddrcss.android.quotedemo.tools.Constant;
import com.ddrcss.android.quotedemo.data.UserInfo;

import okhttp3.Request;

public class UserInfoCall extends ApiCall<UserInfo> {

    public UserInfoCall(String login, String token) {
        super(buildOkRequest(login, token));
    }

    private static Request buildOkRequest(String login, String token) {
        Request request = new Request.Builder().url(Endpoint.getInstance().userInfo(login).toString())
                .header(Constant.HEADER_AUTHORIZATION, Endpoint.formatAuthorization(Constant.API_KEY))
                .header(Constant.HEADER_USER_TOKEN, token)
                .get()
                .build();
        return request;
    }

    public void execute(OnSuccess<UserInfo> onSuccess, OnError onError) {
        super.execute(UserInfo.class, onSuccess, onError);
    }
}

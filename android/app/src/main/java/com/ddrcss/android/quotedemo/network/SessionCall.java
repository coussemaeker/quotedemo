package com.ddrcss.android.quotedemo.network;

import com.ddrcss.android.quotedemo.tools.Constant;
import com.ddrcss.android.quotedemo.tools.Static;
import com.ddrcss.android.quotedemo.data.SessionPayload;
import com.ddrcss.android.quotedemo.data.SessionResult;

import okhttp3.MediaType;
import okhttp3.Request;
import okhttp3.RequestBody;

public class SessionCall extends ApiCall<SessionResult> {

    public SessionCall(String username, String password) {
        super(buildOkRequest(username, password));
    }

    private static Request buildOkRequest(String username, String password) {
        RequestBody body = RequestBody.create(MediaType.parse(Endpoint.HEADER_CONTENT_TYPE_JSON), Static.getGson().toJson(new SessionPayload(username, password), SessionPayload.class));
        Request request = new Request.Builder().url(Endpoint.getInstance().session().toString())
                .header(Constant.HEADER_AUTHORIZATION, Endpoint.formatAuthorization(Constant.API_KEY))
                .post(body)
                .build();
        return request;
    }

    public void execute(OnSuccess<SessionResult> onSuccess, OnError onError) {
        super.execute(SessionResult.class, onSuccess, onError);
    }
}

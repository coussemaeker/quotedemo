package com.ddrcss.android.quotedemo.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.ddrcss.android.quotedemo.R;
import com.ddrcss.android.quotedemo.network.ApiCall;
import com.ddrcss.android.quotedemo.network.SessionCall;
import com.ddrcss.android.quotedemo.tools.Static;

public class UserSigninFragment extends DemoFragment {

    private EditText loginEdit;
    private EditText passwordEdit;
    private Button loginButton;

    public UserSigninFragment() {
        super();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_user_signin, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        loginEdit = view.findViewById(R.id.loginEdit);
        passwordEdit = view.findViewById(R.id.passwordEdit);
        loginButton = view.findViewById(R.id.loginButton);
        loginButton.setOnClickListener(this::loginPressed);
    }

    private void loginPressed(View buttonView) {
        String login = loginEdit.getText().toString();
        String password = passwordEdit.getText().toString();
        new SessionCall(login, password).execute(sessionResult -> {
            Static.getPrefsStorage().save(sessionResult.getLogin(), sessionResult.getToken());
            getParentActivity().loadUserInfoFragment();
        }, (ioe, errorResult) -> {
            Toast.makeText(getActivity(), ApiCall.formatError(ioe, errorResult), Toast.LENGTH_LONG).show();
        });

    }

}

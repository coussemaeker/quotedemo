package com.ddrcss.android.quotedemo.network;

import com.ddrcss.android.quotedemo.data.QuotesPage;
import com.ddrcss.android.quotedemo.tools.Constant;

import okhttp3.Request;

public class QuotesCall extends ApiCall<QuotesPage> {

    public QuotesCall(String login, String token, int pageNum) {
        super(buildOkRequest(login, token, pageNum));
    }

    private static Request buildOkRequest(String login, String token, int pageNum) {
        Request request = new Request.Builder().url(Endpoint.getInstance().favoriteQuotes(login, pageNum).toString())
                .header(Constant.HEADER_AUTHORIZATION, Endpoint.formatAuthorization(Constant.API_KEY))
                .header(Constant.HEADER_USER_TOKEN, token)
                .get()
                .build();
        return request;
    }

    public void execute(OnSuccess<QuotesPage> onSuccess, OnError onError) {
        super.execute(QuotesPage.class, onSuccess, onError);
    }
}

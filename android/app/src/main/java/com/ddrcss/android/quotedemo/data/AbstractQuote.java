package com.ddrcss.android.quotedemo.data;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public abstract class AbstractQuote<B> {

    private final static String PROPERTY_AUTHOR = "author";
    private final static String PROPERTY_BODY = "body";
    private final static String PROPERTY_TAGS = "tags";
    private final static String PROPERTY_FAVORITE = "favorite";
    private final static String PROPERTY_ID = "id";
    private final static String PROPERTY_FAVORITES_COUNT = "favorites_count";
    private final static String PROPERTY_URL = "url";

    @Expose
    @SerializedName(PROPERTY_AUTHOR)
    private String author;

    @Expose
    @SerializedName(PROPERTY_BODY)
    private B body;

    @Expose
    @SerializedName(PROPERTY_TAGS)
    private List<String> tags;

    @Expose
    @SerializedName(PROPERTY_FAVORITE)
    private boolean favorite;

    @Expose
    @SerializedName(PROPERTY_ID)
    private long quoteId;

    @Expose
    @SerializedName(PROPERTY_FAVORITES_COUNT)
    private int favoriteCount;

    @Expose
    @SerializedName(PROPERTY_URL)
    private String url;

    public String getAuthor() {
        return author;
    }

    public B getBody() {
        return body;
    }

    public List<String> getTags() {
        return tags;
    }

    public boolean isFavorite() {
        return favorite;
    }

    public long getQuoteId() {
        return quoteId;
    }

    public int getFavoriteCount() {
        return favoriteCount;
    }

    public String getUrl() {
        return url;
    }

    public abstract void bindBodyToView(LayoutInflater layoutInflater, ViewGroup container);

}

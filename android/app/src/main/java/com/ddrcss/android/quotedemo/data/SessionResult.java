package com.ddrcss.android.quotedemo.data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SessionResult {

    private static final String PROPERTY_LOGIN = "login";
    private static final String PROPERTY_EMAIL = "email";
    private static final String PROPERTY_TOKEN = "User-Token";

    @Expose
    @SerializedName(PROPERTY_LOGIN)
    private String login;

    @Expose
    @SerializedName(PROPERTY_EMAIL)
    private String email;

    @Expose
    @SerializedName(PROPERTY_TOKEN)
    private String token;

    public String getLogin() {
        return login;
    }

    public String getEmail() {
        return email;
    }

    public String getToken() {
        return token;
    }

}

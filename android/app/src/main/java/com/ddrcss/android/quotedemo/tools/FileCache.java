package com.ddrcss.android.quotedemo.tools;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;

import com.ddrcss.android.quotedemo.data.QuoteCacheEntry;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class FileCache {

    private File cacheDir;

    private static final String CACHE_DIRNAME = "quotecache";
    private static final String EXT_TMP = ".tmp";
    private static final String EXT_JSON = ".json";

    public FileCache(Context context) {
        File cacheRoot = context.getCacheDir();
        cacheDir = new File(cacheRoot, CACHE_DIRNAME);
    }

    public boolean save(@NonNull String login, @NonNull QuoteCacheEntry cacheEntry) {
        if (!cacheDir.exists() && !cacheDir.mkdirs()) {
            Log.d(getClass().getName(), "Unable to create cache directory: " + cacheDir.getAbsolutePath());
            return false;
        }
        File cacheFile = new File(cacheDir, login + EXT_TMP);
        try {
            FileWriter fileWriter = new FileWriter(cacheFile);
            Static.getGson().toJson(cacheEntry, fileWriter);
            fileWriter.close();
            cacheFile.renameTo(new File(cacheDir, login + EXT_JSON));
            Log.d(getClass().getName(), "saved to cache file=" + login + EXT_JSON + " itemCount=" + cacheEntry.getQuotes().size());
            return true;
        } catch (IOException ioe) {
            Log.d(getClass().getName(), "IOException", ioe);
            return false;
        }
    }

    public QuoteCacheEntry load(@NonNull String login) {
        File cacheFile = new File(cacheDir, login + EXT_JSON);
        if (!cacheFile.canRead()) {
            Log.d(getClass().getName(), "Unable to read cache file: " + cacheFile.getAbsolutePath());
            return null;
        }
        try {
            FileReader fileReader = new FileReader(cacheFile);
            QuoteCacheEntry cacheEntry = Static.getGson().fromJson(fileReader, QuoteCacheEntry.class);
            fileReader.close();
            Log.d(getClass().getName(), "loaded from cache file= " + cacheFile.getAbsolutePath() + " itemCount=" + cacheEntry.getQuotes().size());
            return cacheEntry;
        } catch (IOException ioe) {
            Log.d(getClass().getName(), "IOException", ioe);
            return null;
        }
    }

}

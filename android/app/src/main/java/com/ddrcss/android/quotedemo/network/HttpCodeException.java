package com.ddrcss.android.quotedemo.network;

import java.io.IOException;

public class HttpCodeException extends IOException {

    private int statusCode;

    public HttpCodeException(int statusCode) {
        super("HTTP status code " + statusCode);
        this.statusCode = statusCode;
    }

    public int getStatusCode() {
        return statusCode;
    }

}

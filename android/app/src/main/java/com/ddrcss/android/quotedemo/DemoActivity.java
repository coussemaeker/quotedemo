package com.ddrcss.android.quotedemo;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.ddrcss.android.quotedemo.fragment.QuotesFragment;
import com.ddrcss.android.quotedemo.fragment.UserInfoFragment;
import com.ddrcss.android.quotedemo.fragment.UserSigninFragment;
import com.ddrcss.android.quotedemo.tools.Constant;
import com.ddrcss.android.quotedemo.tools.Static;

public class DemoActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_account);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        /*
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
        */
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (getMainFragment() == null) {
            loadStartupFragment();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_user_account, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        /*
        if (id == R.id.action_settings) {
            return true;
        }
        */

        return super.onOptionsItemSelected(item);
    }

    private void replaceMainFragment(Fragment fragment) {
        replaceMainFragment(fragment, false);
    }

    private void replaceMainFragment(Fragment fragment, boolean backNavigation) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, fragment, Constant.TAG_MAIN_FRAGMENT);
        if (backNavigation) {
            transaction.addToBackStack(null);
        }
        transaction.commit();
    }

    private Fragment getMainFragment() {
        return getSupportFragmentManager().findFragmentByTag(Constant.TAG_MAIN_FRAGMENT);
    }

    public void loadStartupFragment() {
        if (Static.getPrefsStorage().hasSessionInfo()) {
            loadUserInfoFragment();
        } else {
            loadUserSigninFragment();
        }
    }

    public void loadUserSigninFragment() {
        replaceMainFragment(new UserSigninFragment());
    }

    public void loadUserInfoFragment() {
        replaceMainFragment(new UserInfoFragment());
    }

    public void loadQuotesFragment() {
        replaceMainFragment(new QuotesFragment(), true);
    }

}

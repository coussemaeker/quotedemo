package com.ddrcss.android.quotedemo.data;

import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ddrcss.android.quotedemo.R;

public class Quote extends AbstractQuote<String> {

    public Quote() {
        super();
    }

    @Override
    public void bindBodyToView(LayoutInflater layoutInflater, ViewGroup container) {
        TextView bodyView;
        if (container.getChildCount() == 1 && container.getChildAt(0) instanceof TextView) {
            bodyView = (TextView) container.getChildAt(0);
        } else {
            container.removeAllViews();
            bodyView = (TextView) layoutInflater.inflate(R.layout.view_quote_body_string, container, false);
            container.addView(bodyView);
        }
        bodyView.setText(getBody());
    }

}

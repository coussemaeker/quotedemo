package com.ddrcss.android.quotedemo.network;

import android.net.Uri;

import java.util.Locale;

public class Endpoint {

    private static final String DEFAULT_BASE = "https://favqs.com/api";

    private static final String SEGMENT_QUOTES = "quotes";
    private static final String SEGMENT_USERS = "users";
    private static final String SEGMENT_SESSION = "session";
    private static final String QUERY_FILTER = "filter";
    private static final String QUERY_TYPE = "type";
    private static final String QUERY_VALUE_USER = "user";
    private static final String QUERY_PAGE = "page";

    public static final String HEADER_CONTENT_TYPE = "Content-Type";
    public static final String HEADER_CONTENT_TYPE_JSON = "application/json";
    private static final String HEADER_AUTHORIZATION = "Token token=\"%s\"";

    private static Endpoint instance = new Endpoint(DEFAULT_BASE);

    private Uri baseUri;

    public static Endpoint getInstance() {
        return instance;
    }

    public Endpoint(String base) {
        baseUri = Uri.parse(base);
    }

    private Uri.Builder buildUpon() {
        return baseUri.buildUpon();
    }

    public Uri session() {
        return buildUpon().appendPath(SEGMENT_SESSION).build();
    }

    public Uri userInfo(String login) {
        return buildUpon().appendPath(SEGMENT_USERS).appendPath(login).build();
    }

    public Uri favoriteQuotes(String login, int pageNum) {
        Uri.Builder uriBuilder = buildUpon().appendPath(SEGMENT_QUOTES).appendQueryParameter(QUERY_FILTER, login).appendQueryParameter(QUERY_TYPE, QUERY_VALUE_USER);
        if (pageNum > 1) {
            uriBuilder.appendQueryParameter(QUERY_PAGE, Integer.toString(pageNum));
        }
        return uriBuilder.build();
    }

    public static String formatAuthorization(String apiKey) {
        return String.format(Locale.US, HEADER_AUTHORIZATION, apiKey);
    }

}

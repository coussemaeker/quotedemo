package com.ddrcss.android.quotedemo.network;

import android.os.Handler;
import android.util.Log;

import com.ddrcss.android.quotedemo.data.ErrorResult;
import com.ddrcss.android.quotedemo.tools.Static;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Request;
import okhttp3.Response;

public class ApiCall<T> {

    private Handler uiThreadHandler;
    private Request okRequest;

    public static interface OnSuccess<T> {
        void onSuccess(T result);
    }

    public static interface OnError {
        void onError(IOException ioe, ErrorResult errorResult);
    }

    public ApiCall(Request okRequest) {
        this.okRequest = okRequest;
        uiThreadHandler = new Handler();
    }

    public void execute(Class<T> classOfT, OnSuccess<T> onSuccess, final OnError onError) {
        Log.d(getClass().getName(), "queue request for url=" + okRequest.url().toString());
        Static.getOkHttpClient().newCall(okRequest).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException ioe) {
                uiThreadHandler.post(() -> onError.onError(ioe, null));
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {

                int responseCode = response.code();
                if (responseCode != 200) {
                    onFailure(call, new HttpCodeException(responseCode));
                    return;
                }
                String contentType = response.header(Endpoint.HEADER_CONTENT_TYPE);
                if (contentType == null || !contentType.startsWith(Endpoint.HEADER_CONTENT_TYPE_JSON)) {
                    onFailure(call, new IOException("unsupported Content-Type"));
                }
                String responseString = response.body().string();
                ErrorResult errorResult = Static.getGson().fromJson(responseString, ErrorResult.class);
                if (errorResult.isError()) {
                    uiThreadHandler.post(() -> onError.onError(null, errorResult));
                    return;
                }
                T result = Static.getGson().fromJson(responseString, classOfT);
                uiThreadHandler.post(() -> onSuccess.onSuccess(result));
            }
        });
    }

    public static String formatError(IOException ioe, ErrorResult errorResult) {
        StringBuffer sb = new StringBuffer();
        if (ioe != null) {
            sb.append("Exception: ");
            sb.append(ioe.getClass().getSimpleName());
            if (ioe.getMessage() != null) {
                sb.append(" message=");
                sb.append(ioe.getMessage());
            }
        }
        if (errorResult != null) {
            if (sb.length() > 0) {
                sb.append("\n");
            }
            sb.append("errorCode=");
            sb.append(errorResult.getErrorCode());
            sb.append(" errorMessage=");
            sb.append(errorResult.getMessage());
        }
        return sb.toString();
    }

}

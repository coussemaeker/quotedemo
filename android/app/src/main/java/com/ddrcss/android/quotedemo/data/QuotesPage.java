package com.ddrcss.android.quotedemo.data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class QuotesPage {

    private static final String PROPERTY_PAGE = "page";
    private static final String PROPERTY_LAST_PAGE = "last_page";
    private static final String PROPERTY_QUOTES = "quotes";

    @Expose
    @SerializedName(PROPERTY_PAGE)
    private int pageNumber;

    @Expose
    @SerializedName(PROPERTY_LAST_PAGE)
    private boolean lastPage;

    @Expose
    @SerializedName(PROPERTY_QUOTES)
    private List<Quote> quotes;

    public int getPageNumber() {
        return pageNumber;
    }

    public boolean isLastPage() {
        return lastPage;
    }

    public List<Quote> getQuotes() {
        return quotes;
    }

}

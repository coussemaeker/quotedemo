package com.ddrcss.android.quotedemo.tools;

public class Constant {

    private Constant() {
        // force static use
    }

    public static final String API_KEY = "80ed87166a12b9be650ffbc082952847";

    public static final String TAG_MAIN_FRAGMENT = "MAIN_FRAGMENT";
    public static final String TAG_ALERT_FRAGMENT = "ALERT_FRAGMENT";

    public static final String HEADER_AUTHORIZATION = "Authorization";
    public static final String HEADER_USER_TOKEN = "User-Token";

}
